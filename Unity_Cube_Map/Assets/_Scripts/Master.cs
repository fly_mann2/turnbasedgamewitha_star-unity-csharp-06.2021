using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Hold map information for different scenes 
public static class Master 
{
    // map
    public static LevelGenerator levelGenerator;
    public static Map map;
    public static Texture2D mapImage;
    private static Vector2Int mapSize;
    public static MapPathfinding pathfinding;
    // Game Loop
    public static Controller controller;
    
    // Object DB
    public static DBComponent dbComponent;
    // Other
    public static PlayerController playerCon;

    public static void Init(DBComponent _DBCom, LevelGenerator _lvl, Map _map, Controller _con){
        dbComponent = _DBCom;
        levelGenerator = _lvl;
        Master.mapSize = levelGenerator.GetMapSize();
        map = _map;
        controller = _con;
        pathfinding = new MapPathfinding(map);
        playerCon = new PlayerController(map, Master.mapSize);

        controller.Init(dbComponent);
    }
}
