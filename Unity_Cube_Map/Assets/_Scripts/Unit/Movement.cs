using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{   
    List<Tile> path = new List<Tile>();
    public Transform looker, target;

    MapPathfinding pathFinder;
    private bool canMove = false;

    public bool doneWithTurn = true;

    void Start()
    {
        looker = transform;

        GameObject.FindWithTag("Master").GetComponent<Controller>().teamUnits.units.Add(this);
        pathFinder = Master.pathfinding;
        
        target = GameObject.FindWithTag("Player").transform;

        
    }

    public void UpdatePathForUnit(){
        path = pathFinder.FindPath(looker.position, target.position);
        pathFinder.ShowPath(path);
    }
    public void MoveOne(){
        
        if(path.Count > 1) { canMove = true; }
        else{ canMove = false; }
        if(canMove){
            transform.position = path[0].worldPosition;
        }
        pathFinder.ShowPath(path);
    }
    public void StartTimer(){
        doneWithTurn = false;
        StartCoroutine(ExampleCoroutine(0.001f));
    }
    
    IEnumerator ExampleCoroutine(float _wait)
    {
        yield return new WaitForSeconds(_wait);
        doneWithTurn = true;
    }
}
