using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeamUnits 
{
    public List<Movement> units = new List<Movement>();
    int index = 0;

    bool canMoveUnit = true;

    public void Init(Controller _controller){
        foreach (var unit in units)
        {
            //unit.Init(ref _controller);
        }
    }
    public bool MoveUnits(){
        if(units.Count > 0){
            // Find path
            FindPathAll();
            if(canMoveUnit && units[index].doneWithTurn){
                canMoveUnit = false;
                units[index].MoveOne();
                units[index].StartTimer();
            }
            
        }
        else {
            return true;
        }
        if(units[index].doneWithTurn && canMoveUnit == false){
            canMoveUnit = true;
            // Update path
            FindPathAll();
            return units[index].doneWithTurn;
        }
        return false;
    }
    public int SelectNextUnit() {
        if (index < units.Count - 1)
        {
            index++;
        }
        else{
            index = 0;
        }
        return index;
    }
    
    public void FindPathAll(){
        foreach (var unit in units)
        {
            unit.UpdatePathForUnit();
        }
    }
}
