using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floor : MonoBehaviour
{
    // Start is called before the first frame update
    public Material water;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter(Collider collider)
    {
        if(collider.CompareTag("Enemy")){
            GetComponent<MeshRenderer>().material = water;
            gameObject.layer = 7;

            Movement move = collider.gameObject.GetComponent<Movement>();
            GameObject.FindWithTag("Master").GetComponent<Controller>().teamUnits.units.Remove(move);
            collider.gameObject.GetComponent<Rigidbody>().useGravity = true;

            Destroy(collider.gameObject, 2);
            this.enabled = false;
        }
    }
}
