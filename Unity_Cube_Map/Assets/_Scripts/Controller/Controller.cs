using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    // All enemies 
    public TeamUnits teamUnits = new TeamUnits();
    public DBComponent objDB;
    // Wait for PlayerInput
    public bool waitForInput = true;


    public void Init(DBComponent _DBCom)
    {
        objDB =  Master.dbComponent;

        // Init
        objDB.levelGenerator.GenerateLevel(this);
        objDB.map.InitMap(objDB.levelGenerator);

        //pathfinding = new MapPathfinding(objDB.map);
    }

    void Update()
    {
        // Input
        if(waitForInput) {
            if(Input.GetKey(KeyCode.T)){
                waitForInput = false;
            }
            if(Input.GetKeyDown(KeyCode.R)){
                waitForInput = false;
            }
            Master.playerCon.MouseUpdate();
            teamUnits.FindPathAll();
        }
        // Game
        else if(!waitForInput){
            
            if(teamUnits.MoveUnits()){
                waitForInput = true;
                objDB.map.UpdateMap();
                if(teamUnits.SelectNextUnit() == 0) { objDB.map.path = new List<Tile>();  }
            }
        }
        // UI
    }
    
    
}
