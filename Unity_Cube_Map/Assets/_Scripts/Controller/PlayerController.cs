using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController //: MonoBehaviour
{
    private static Vector2Int mapSize;
    Ray ray;

    Map map;

    int index = 0;
    // Give PlayerController access to map
    public PlayerController(Map _map, Vector2Int _mapSize){
        map = _map;
        mapSize = _mapSize;
    }
    
    public void MouseUpdate()
    {
        Mousescroll();
        SpawnObjWithMouse();

        
    }
    // Click on map to spawn object
    void SpawnObjWithMouse(){
        if(Input.GetKeyDown(KeyCode.Mouse0)){
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitData;
            Vector3 worldPosition = new Vector3(1,0,1);

            if(Physics.Raycast(ray, out hitData, 1000))
            {
                worldPosition = hitData.point;
                worldPosition.x = Mathf.RoundToInt(worldPosition.x);
                worldPosition.z = Mathf.RoundToInt(worldPosition.z);
                worldPosition.y = 0.5f;

                // Check within map AND tile not block by wall
                if(    worldPosition.x >= 0 && worldPosition.x < mapSize.x
                    && worldPosition.z >= 0 && worldPosition.z < mapSize.y
                    && map.GetTileAtPos(new Vector3 (worldPosition.x, 0, worldPosition.z)).wall == false){
                        Master.dbComponent.SpawnObjIntoWorld(index, worldPosition);
                        map.UpdateMap();
                }
            }
            
        }
    }
    // Select object to spawn with mouse click
    void Mousescroll(){
        if (Input.GetAxis("Mouse ScrollWheel") > 0f) // forward
        {
             if(index >= Master.dbComponent.SpawnObj.Length -1){
                 index = 0;
             }
             else{
                 index++;
             }
        }
        if (Input.GetAxis("Mouse ScrollWheel") < 0f) // backwards
        {
            if(index <= 0){
                 index = Master.dbComponent.SpawnObj.Length -1;
             }
             else{
                 index--;
             }
        }
    }
}
