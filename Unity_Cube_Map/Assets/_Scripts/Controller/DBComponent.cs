using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DBComponent : MonoBehaviour
{
    public Controller controller;
    public Map map;
    public LevelGenerator levelGenerator;
    public GameObject[] SpawnObj;

    public ColorToPrefab[] colorMappings;

    void Awake()
    {
        //  DBComponent
        GameObject masterObj = GameObject.FindWithTag("Master");
        controller = masterObj.GetComponent<Controller>();
        levelGenerator = masterObj.GetComponent<LevelGenerator>();
        map = masterObj.GetComponent<Map>();
        // Master
        Master.dbComponent = this;

        Master.Init(this, levelGenerator, map, controller);
        
    }
    public void SpawnObjIntoWorld(int _index, Vector3 _worldPos){
        GameObject go = Instantiate(Master.dbComponent.SpawnObj[_index], _worldPos, Quaternion.identity);
    }
}
