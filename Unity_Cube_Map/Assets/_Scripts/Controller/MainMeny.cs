using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using UnityEngine.UI;


public class MainMeny : MonoBehaviour
{
    private int index = 0;
    public List<string> mapNames = new List<string>();
    const string path = "./Assets/Resources/Map";
    public Texture2D map;
    public Image mapPic;
    public void PlayButton(){
        Master.mapImage = map;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        
    }
    private void Start() {
        string [] files = Directory.GetFiles (path, "*.*");
        foreach (string sourceFile in files)
        {
            string fileName = Path.GetFileName (sourceFile);
            if(fileName.Substring(fileName.Length - 4).Contains("png")){
                mapNames.Add(fileName);
                Debug.Log(fileName);
            }
            
        }
        map = LoadImage(path  + "/" + mapNames[index]);
        mapPic.sprite = Sprite.Create(map, new Rect(0.0f,0.0f,map.width,map.height), new Vector2(0.5f,0.5f), 100.0f);
    }
    public void IndexPlus(){
        if(index < mapNames.Count -1){
            index++;
        }
        else{
            index = 0;
        }
    }
    public void IndexMinus(){
        if(index <= 0){
            index = mapNames.Count -1;
        }
        else{
            index--;
        }
    }
    public void NextButton(){
        IndexPlus();
        print(path  + "/" + mapNames[index]);
        map = LoadImage(path  + "/" + mapNames[index]);
        mapPic.sprite = Sprite.Create(map, new Rect(0.0f,0.0f,map.width,map.height), new Vector2(0.5f,0.5f), 100.0f);
        
    }
    public void BackButton(){
        IndexMinus();
        map = LoadImage(path  + "/" + mapNames[index]);
        mapPic.sprite = Sprite.Create(map, new Rect(0.0f,0.0f,map.width,map.height), new Vector2(0.5f,0.5f), 100.0f);
    }
    public Texture2D LoadImage(string filePath){
        Texture2D tex = null;
        byte[] fileData;
    
        if (File.Exists(filePath))     {
            fileData = File.ReadAllBytes(filePath);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
        }
        return tex;
    }
}
