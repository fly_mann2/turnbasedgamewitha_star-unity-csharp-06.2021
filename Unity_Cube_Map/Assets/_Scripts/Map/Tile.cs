﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Tile {
	
	public Vector3 worldPosition;
	public int posX;
	public int posY;
	public int gCost;
	public int hCost;
	public Tile parent;
	public bool wall = false;
	
	public Tile(bool _wall, Vector3 _worldPos,  int _gridY, int _gridX) {
		this.wall = _wall;
		this.worldPosition = _worldPos;
		this.posX = _gridX;
		this.posY = _gridY;
	}

	public int fCost {
		get {
			return gCost + hCost;
		}
	}
}
