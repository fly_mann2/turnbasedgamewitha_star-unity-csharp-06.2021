using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map : MonoBehaviour
{
    // Show A* in editor 
    public bool drawGizmos = true;
    // Check tile Layer eg. wall
    public LayerMask[] tileType;
	public Vector2Int mapSize;
	// size of nodes: must be one right now
	float tileRadius = 0.5f;
    // nodeRadius * 2
    float tileRadiusDobbel;
    // Map Size
    int mapSizeX, mapSizeY;
	Tile[,] map;
    public void InitMap(LevelGenerator _levelGenerator){

        mapSize = _levelGenerator.GetMapSize();
        tileRadiusDobbel = tileRadius * 2;
        
        mapSizeX = Mathf.RoundToInt(mapSize.x/tileRadiusDobbel);
		mapSizeY = Mathf.RoundToInt(mapSize.y/tileRadiusDobbel);

        UpdateMap();
        
    }
    // Create A* map
    public void UpdateMap() {
		map = new Tile[mapSize.y, mapSize.x];

        for (int z = 0; z < mapSize.y; z++)
        {
            for (int x = 0; x < mapSize.x; x++)
            {
                Vector3 worldPos = new Vector3(x * tileRadiusDobbel + tileRadius,0,z * tileRadiusDobbel + tileRadius) + 
                    transform.position;
                // Check if wall
                bool walled = (Physics.CheckSphere(worldPos, tileRadius -0.01f, tileType[0] ));
                // Check if enemy
                if(!walled) { walled = (Physics.CheckSphere(worldPos, tileRadius -0.01f, tileType[1] )); }
                map[z,x] = new Tile(walled, worldPos, z,x);
            }
        }
	}
    // World postion to int. Then used as array index
    public Tile GetTileAtPos(Vector3 worldPosition){
       int z = Mathf.RoundToInt(worldPosition.z);
       int x = Mathf.RoundToInt(worldPosition.x);
       if(z >= mapSizeY || x >= mapSizeX ||
            z < 0 || x < 0){
           return null;
       }
        return map[z, x];
    }
    // Tile x 9 - self
    public List<Tile> GetTileNeighbor(Tile tile) {
		List<Tile> neighbors = new List<Tile>();

        for (int y = -1; y <= 1; y++) {
		    for (int x = -1; x <= 1; x++) {
				if (y == 0 && x == 0)
                    // self ignore
					continue;

				int getAtY = tile.posY + y;
				int getAtX = tile.posX + x;

				if (getAtY >= 0 && getAtY < mapSizeY && 
                    getAtX >= 0 && getAtX < mapSizeX) {
					neighbors.Add(map[getAtY,getAtX]);
				}
			}
		}

		return neighbors;
	}

    // Show A* path
    public List<Tile> path = new List<Tile>();
    void OnDrawGizmos() {
        // Color mening: white=Walk, red=Wall, blue=Path 
        if (drawGizmos && map != null) {
            foreach (Tile n in map) {
                if(n.wall){ Gizmos.color = Color.red; }
                else { Gizmos.color = Color.white; }

                if (path != null)
                    if (path.Contains(n)){
                        Gizmos.color = Color.blue;
                    }
                Gizmos.DrawCube(n.worldPosition + new Vector3(0,-0.4f, 0), Vector3.one * (tileRadiusDobbel-.6f));
            }
        }
    }
}
