using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapPathfinding 
{
	Map map;

	public MapPathfinding(Map _map) {
		
		this.map = _map;
	}
	// A*
	public List<Tile> FindPath(Vector3 startPos, Vector3 targetPos) {
		Tile startTile = map.GetTileAtPos(startPos);
		Tile targetTile = map.GetTileAtPos(targetPos);
        

		List<Tile> openSet = new List<Tile>();
		HashSet<Tile> closedSet = new HashSet<Tile>();

        openSet.Add(startTile);

		while (openSet.Count > 0) {

			Tile tile = openSet[0];
			for (int i = 1; i < openSet.Count; i ++) {
				if (openSet[i].fCost < tile.fCost || openSet[i].fCost == tile.fCost) {
					if (openSet[i].hCost < tile.hCost)
						tile = openSet[i];
				}
			}

			openSet.Remove(tile);
			closedSet.Add(tile);

            
			if (tile == targetTile) {
				return RetracePath(startTile,targetTile);
			}

			foreach (Tile neighbor in map.GetTileNeighbor(tile)) {
				if (neighbor.wall || closedSet.Contains(neighbor)) {
					continue;
				}

				int newCostToNeighbor = tile.gCost + GetDistance(tile, neighbor);
				if (newCostToNeighbor < neighbor.gCost || !openSet.Contains(neighbor)) {
					neighbor.gCost = newCostToNeighbor;
					neighbor.hCost = GetDistance(neighbor, targetTile);
					neighbor.parent = tile;

					if (!openSet.Contains(neighbor))
						openSet.Add(neighbor);
				}
			}
		}
		return null;
	}
	// Return best path
	List<Tile> RetracePath(Tile startTile, Tile endTile) {
		List<Tile> path = new List<Tile>();
		Tile currentTile = endTile;

		while (currentTile != startTile) {
			path.Add(currentTile);
			currentTile = currentTile.parent;
		}
		path.Reverse();

		//map.path = path; // remove
		ShowPath(path);

		return path;
	}
	// Get distance between tiles
	int GetDistance(Tile tileA, Tile tileB) {
		int distanceX = Mathf.Abs(tileA.posX - tileB.posX);
		int distanceY = Mathf.Abs(tileA.posY - tileB.posY);

		if (distanceX > distanceY)
			return 14*distanceY + 10* (distanceX-distanceY);
		return 14*distanceX + 10 * (distanceY-distanceX);
	}
	// Add Tile to Map.path
	public void ShowPath(List<Tile> _path) {
		foreach (Tile n in _path) {
				if (!map.path.Contains(n)){
					map.path.Add(n);
				}
        	}
	}
	
	
}
