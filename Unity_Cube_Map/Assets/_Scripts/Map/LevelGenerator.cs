using UnityEngine;

public class LevelGenerator : MonoBehaviour {
	// Sprite image to create map
	public Texture2D mapSprite;
	// map size x,y
	Vector2Int mapSize;
	// Get size of the level
	public Vector2Int GetMapSize(){
		return new Vector2Int(mapSprite.width, mapSprite.height);
	}
	// Create level
	public void GenerateLevel (Controller _controller)
	{
		// Has public image from editor, will try to get from Master
		if(Master.mapImage != null){
			mapSprite = Master.mapImage;
		}

		for (int x = 0; x < mapSprite.width; x++)
		{
			for (int y = 0; y < mapSprite.height; y++)
			{
				GenerateTile(x, y);
			}
		}
	}
	// Create Object on tile
	void GenerateTile (int x, int y)
	{
		Color pixelColor = mapSprite.GetPixel(x, y);

		if (pixelColor.a == 0)
		{
			// Pixel transparrent = ignore
			Vector3 position = new Vector3(x,  0, y);
			Instantiate(Master.dbComponent.colorMappings[0].prefab, position, Quaternion.identity, transform);
			return;
		}

		foreach (ColorToPrefab colorMapping in Master.dbComponent.colorMappings)
		{
			if (colorMapping.color.Equals(pixelColor))
			{
				Vector3 position = new Vector3(x,  0, y);
				GameObject go = Instantiate(colorMapping.prefab, position, Quaternion.identity, transform);
				
			}
		}
	}
	
}
