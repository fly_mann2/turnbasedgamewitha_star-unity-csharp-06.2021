# Turn Based Game

### Om prosjektet

	Programmet henter bilder fra mappen Resources.
	Leser pixlene og laster in modeller basert på pixel fargen.
	Et kart blir generert for A* algoritme som sjekker for vegger osv.
	Fiender bruker A* algoritme for å komme frem til målet.

### Installasjon og bruk

### Kjøre .exe

	Inni builds mappen finner du Unity_Cube_Map.exe.

### Åpne prosjektet med bruk av Unity

	Installer Unity engine.
	Åpne prosjektet med Unity Hub.
	Trykk på mappen Assets -> Scenes -> SampleScene.unity.
	Trykk på Play knappen toppen av skjermen.

### Kontroller

	Trykk "R" for å flytte Fiender et skritt nærmere målet.
	Hold inne "T" for mange skritt.


### Utviklet med

	Unity | C# | VsCode 

	Algoritmer 
		A*




